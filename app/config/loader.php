<?php

$loader = new \Phalcon\Loader();
$eventsManager = new \Phalcon\Events\Manager();
/**
 * We're a registering a set of directories taken from the configuration file
 */
$loader->registerDirs(
    [
        $config->application->controllersDir,
        $config->application->modelsDir,
        $config->application->pluginsDir,
        $config->application->libraryDir,
    ]
)->register();

$loaderLibrary = new Phalcon\Loader();

$loaderLibrary->registerNamespaces(array(
    'Phalcon' => $config->application->libraryDir,
    'Phalcon\Plugin' =>  $config->application->pluginsDir,

));


$loaderLibrary->setEventsManager($eventsManager);


$loaderLibrary->register();