<?php
/**
 * Services are globally registered in this file
 *
 * @var \Phalcon\Config $config
 */

use Phalcon\Mvc\View;
use Phalcon\Mvc\Url as UrlResolver;
use Phalcon\Db\Adapter\Pdo\Mysql as DbAdapter;
use Phalcon\Mvc\View\Engine\Volt as VoltEngine;
use Phalcon\Mvc\Model\Metadata\Memory as MetaDataAdapter;
use Phalcon\Session\Adapter\Files as SessionAdapter;
use Phalcon\Mvc\Router;
use Phalcon\Plugin;
use Phalcon\Http\Response\Cookies;

use Phalcon\Flash\Direct as FlashDirect;

/**
 * The FactoryDefault Dependency Injector automatically register the right services providing a full stack framework
 */


/**
 * Shared configuration service
 */
$di->setShared('config', function () {
    return include APP_PATH . "/config/config.php";
});


/**
 * The URL component is used to generate all kind of urls in the application
 */
$di->set('url', function () {
    $config = $this->getConfig();
    $url = new UrlResolver();
    $url->setBaseUri($config->application->baseUri);

    return $url;
}, true);

$di->set('availableLanguages',function(){
    $config = $this->getConfig();
    return $config->application->availableLanguages;
},true);

/**
 * All requests go trough security dispatcher
 */

$di->set('dispatcher', function() use ($di) {
    $eventsManager = $di->getShared('eventsManager');
    $dispatcher = new Phalcon\Mvc\Dispatcher();
    $security = new Security($di);
    $eventsManager->attach('dispatch', $security);

    $dispatcher->setEventsManager($eventsManager);

    return $dispatcher;
});



/**
 * Setting up the view component
 */
$di->set('view', function () {

    $config = $this->getConfig();

    $view = new View();

    $view->setViewsDir($config->application->viewsDir);

    $view->registerEngines(array(
        '.volt' => function ($view, $di) use ($config) {

            $volt = new VoltEngine($view, $di);

            $volt->setOptions(array(
                'compiledPath' => $config->application->cacheDir,
                'compiledSeparator' => '_',
                'compileAlways' => true
            ));

            $volt->getCompiler()->addFunction('_', function ($resolvedArgs, $exprArgs) use ($di) {

                return sprintf('$this->translate->query(%s)', $resolvedArgs);
            });
            $volt->getCompiler()->addFunction('__', function ($resolvedArgs, $exprArgs) use ($di) {


                return sprintf('$this->translate->query2(%s)', $resolvedArgs);
            });

            $volt->getCompiler()->addFunction(
                'contains_text',
                function ($resolvedArgs, $exprArgs) {
                    if (function_exists('mb_stripos')) {
                        return 'mb_stripos(' . $resolvedArgs . ')';
                    } else {
                        return 'stripos(' . $resolvedArgs . ')';
                    }
                });

            $volt->getCompiler()->addFunction('in_array', 'in_array');

            $volt->getCompiler()->addFunction("round","round");
            $volt->getCompiler()->addFunction("roundDown",function($resolvedArgs, $exprArgs){
                return 'round('.$resolvedArgs.',PHP_ROUND_HALF_DOWN)';
            });
            $volt->getCompiler()->addFunction("formatNumber",function($resolvedArgs, $exprArgs){
                return 'sprintf("%01.2f", ((mb_strpos((string) '.$resolvedArgs.',"."))?mb_strcut('.$resolvedArgs.',0,mb_strpos((string) '.$resolvedArgs.',".")+3):'.$resolvedArgs.'))';

            });
            $volt->getCompiler()->addFunction("formatNumberZero",function($resolvedArgs, $exprArgs){
                return 'number_format('.$resolvedArgs.', 1, \'.\', \'\');';

            });

            $volt->getCompiler()->addFunction("formatDateForCalendar",function($resolvedArgs, $exprArgs){
                return "";
            });


            $volt->getCompiler()->addFilter('trans', function ($resolvedArgs, $exprArgs) use ($di) {
                return sprintf('$this->translate->query(%s)', $resolvedArgs);
            });


            $volt->getCompiler()->addFunction('countryByIso', function ($resolvedArgs, $exprArgs) use ($di) {
                return sprintf('$this->_user->countryByCode(%s)', $resolvedArgs);
            });

            $volt->getCompiler()->addFilter('trans2', function ($resolvedArgs, $exprArgs) use ($di) {
                return sprintf('$this->translate->query2(%s)', $resolvedArgs);
            });

            $volt->getCompiler()->addFilter('fromUnixtime', function ($resolvedArgs, $exprArgs) use ($di) {
                return sprintf('date("d.m.Y",%s)',$resolvedArgs);
            });
            $volt->getCompiler()->addFilter('timeFromUnixtime', function ($resolvedArgs, $exprArgs) use ($di) {
                return sprintf('date("H:i",%s)',$resolvedArgs);
            });
            $volt->getCompiler()->addFilter('yearFromUnixtime', function ($resolvedArgs, $exprArgs) use ($di) {
                return sprintf('date("Y",%s)',$resolvedArgs);
            });



            return $volt;
        }
    ));

    return $view;
}, true);





/**
 * Encryption functionality (mostly for cookies)
 */

$di->set('crypt', function (){
    $config = $this->getConfig();

    $crypt = new Phalcon\Crypt();
    $crypt->setKey($config->application->encryptKey);
    return $crypt;
} );


$di->setShared('translate', function() use($di) {
    return new Translate();
});



/**
 * Database connection is created based in the parameters defined in the configuration file
 */
$di->set('db', function (){
    $config = $this->getConfig();
    return new DbAdapter($config->database->toArray());
});

/**
 * If the configuration specify the use of metadata adapter use it or use memory otherwise
 */
$di->set('modelsMetadata', function () {
    return new MetaDataAdapter();
});

$di->setShared('notify', function(){
    $config = $this->getConfig();
    $notify = new Notify();
    $notify->setMailConf($config->mail);
    $notify->setSmsConf($config->sms);
    return $notify;
});

$di->setShared('_user', function(){
    $userClass = new UserFunctions();   //new UserFunctions();
    return $userClass;
});

/**
 * User helper functions
 */
/*
$di->setShared('_user',function(){
    $user = new uHelper();
    return $user;
});
*/



/**
 * Start the session the first time some component request the session service
 */
$di->setShared('session', function () {
    $session = new SessionAdapter();
    $session->start();

    return $session;
});
$di->set(
    "cookies",
    function () {
        $cookies = new Cookies();

        $cookies->useEncryption(true);

        return $cookies;
    }
);

$di->setShared('security', function () {
    $security = new \Phalcon\Security;
    return $security;
});


/**
 * Router definition
 */
/*
$di->set(
    'router', function () {
    $router = new Router();
    $router->notFound(array(
        "controller" => "error",
        "action" => "pageNotFound"
    ));
    return $router;
}

);*/