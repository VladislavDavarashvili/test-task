<?php

use Phalcon\Mvc\Controller;

class ControllerBase extends Controller
{
    public function initialize()
    {

        if (!$this->session->has('lang')) {
            $this->session->set("lang", $this->config->application->defaultLang);
        }

        if($this->session->has('userId') && $this->session->get('userId')>0){
            $userObj = User::findFirst(array(
                'conditions' => 'userId=?1',
                'bind' => array(
                    1 => $this->session->get('userId')
                )
            ));
            if(!$userObj){
                //Redirect to error page
                //$this->session->remove('userId');
                $this->response->redirect('/error/pageNotFound');


            }else{
                $this->view->setVar('userObj',$userObj);
            }
        }else{
            if($this->cookies->has('rememberMe') && $this->cookies->get('rememberMe') == 1){
                $email = $this->cookies->get('email');
                $passw = $this->cookies->get('password');
                $user = User::findFirst(array(
                                'conditions' => 'email=?1',
                                'bind' => array(
                                    1 => $email
                                )
                            ));

                if(!$user){
                    $this->cookies->get('rememberMe')->delete();
                    $this->cookies->get('password')->delete();
                    $this->cookies->get('email')->delete();
                }else{

                    if($passw == $user->password){
                        $this->session->set('userId',$user->userId);
                    }else{
                        $this->cookies->get('rememberMe')->delete();
                        $this->cookies->get('password')->delete();
                        $this->cookies->get('email')->delete();
                    }
                }

            }
        }
    }
}
