<?php

class ErrorController extends ControllerBase
{

    public function indexAction()
    {

    }

    public function pageNotFoundAction(){
        Phalcon\Tag::setTitle($this->translate->query("Ooops! Page is not found"));
    }
    public function pageIsNotRespondingAction(){
        Phalcon\Tag::setTitle($this->translate->query("Ooops! Page is not found"));

        if($this->request->isAjax()){
            print json_encode(array('status' => 'fail', 'messages' => $this->translate->query("Error occurred. Please tr again later")));
            $this->view->disable();
            $this->response->setContentType('application/json', 'UTF-8');
        }
        $exction = $this->dispatcher->getParam('exception');

        $this->view->setVar('errorMessageForAdmin',$exction);

        $errorMessage = "
        Exception: ".$exction." <BR>
        Controller: ". $this->dispatcher->getParam('controllerName')."<BR>
        Action: ".$this->dispatcher->getParam('actionName')."<BR>
        file: ".$exction->getFile()."<BR>
        line: ".$exction->getLine()."<BR>
        trace: ".$exction->getTraceAsString()."<BR>
        ";
        $this->view->setVar('outputMessage',$errorMessage);
        //$this->notify->sendDebug(array("message"=>$errorMessage));
    }
}

