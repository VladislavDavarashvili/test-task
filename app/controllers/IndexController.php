<?php
use Phalcon\Mvc\View,
    Phalcon\Paginator\Adapter\QueryBuilder as PaginatorQueryBuilder;
use Phalcon\Paginator\Adapter\Model as PaginatorModel;
class IndexController extends ControllerBase
{

    public function indexAction()
    {
        Phalcon\Tag::setTitle($this->translate->query("Welcome to our home page"));


        $code = $this->_user->generateRandomString(7).time();
        //$this->notify->sendDebug(array("message"=>"whooohooo"));
    }

    public function debugAction(){
        echo 'email:'.$this->cookies->get('email');

        $this->view->disable();
    }


}

