<?php

class UserController extends ControllerBase
{

    public function indexAction()
    {
        $this->dispatcher->forward(array(
            'controller' => 'user',
            'action' => 'profile'
        ));
    }

    public function registerAction(){
        Phalcon\Tag::setTitle($this->translate->query("Register"));
        $this->view->setVar('isDarkackgroud',true);

        if($this->request->isPost()){
            $firstname = $this->request->getPost('firstname','string');
            $lastname = $this->request->getPost('lastname','string');
            $email = $this->request->getPost('email','email');
            $password = $this->request->getPost('password');
            $password2 = $this->request->getPost('password2');

            $errorArray = array();
            if(mb_strlen($firstname)<2){
                $errorArray['firstname'] = $this->translate->query("Firstname is too short");
            }
            if(mb_strlen($lastname)<2){
                $errorArray['lastname'] = $this->translate->query("Lastname is too short");
            }

            if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
                $emailCheck = User::find(array(
                    'conditions' => 'email=?1',
                    'bind' => array(
                        1 => $email
                    )
                ));
                if(count($emailCheck) > 0){
                    $errorArray['email'] = $this->translate->query("E-mail is in use");
                }
            }else{
                $errorArray['email'] = $this->translate->query("E-mail is not valid");
            }

            if(mb_strlen($password)<6){
                $errorArray['password'] = $this->translate->query("Password is too short");
            }

            if($password != $password2){
                $errorArray['password'] = $this->translate->query("Passwords do not match");
            }

            if($this->request->getPost('iConfirm') != 1){
                $errorArray['iConfirm'] = $this->translate->query("Accept terms&conditions");
            }

            if(count($errorArray)==0){
                $createUser = new User();
                $createUser->firstname = $firstname;
                $createUser->lastname = $lastname;
                $createUser->email = $email;
                $createUser->password = $this->security->hash($this->request->getPost('password'));
                $createUser->emailHash = $this->_user->generateRandomString(7).time();
                $createUser->language = $this->session->get('lang');
                $createUser->createDate = time();
                if(!$createUser->save()){

                    if(!$this->request->isAjax()){

                        $this->dispatcher->forward(array(
                            'controller' => 'error',
                            'action' => 'pageIsNotResponding'
                        ));


                    }else{
                        print json_encode(array('status' => 'fail', 'messages' => array($this->translate->query("Ooops! Something went wrong"))));
                    }

                }else{
                    try {
                        $this->notify->sendEmail($createUser->email, $this->translate->query("Registration_mail_title"), array('user','registration'), array(), array('userObj'=>$createUser));
                    }catch(\Swift_TransportException $e){

                    }
                    print json_encode(array('status' => 'success', 'isMessage' => false, 'message' => '', 'url' => '/user/profile'));
                }

            }else{
                print json_encode(array('status' => 'fail', 'messages' => $errorArray));
            }
        }

        if($this->request->isAjax()){
            $this->view->disable();
            $this->response->setContentType('application/json', 'UTF-8');
        }else{
            /* TODO: add request vars to display in form fields. + error highlights  */
            foreach ($errorArray as $error){
                $this->flashSession->error($error);
            }
        }

    }

    public function loginAction(){
        Phalcon\Tag::setTitle($this->translate->query("Login"));
        $this->view->setVar('isDarkackgroud',true);

        if($this->request->isPost()){
            $email = $this->request->getPost('email','email');
            $password = $this->request->getPost('password');

            $user = User::findFirst(array(
                'conditions' => 'email=?1',
                'bind' => array(
                    1 => $email
                )
            ));
            if(!$user){
                print json_encode(array('status' => 'fail', 'messages' => array($this->translate->query("Please check your email"))));
            }else{
                if($this->security->checkHash($this->request->getPost('password'),$user->password)){


                    if($this->request->getPost('rememberMe') == 1){
                        $this->cookies->set('rememberMe',1);
                        $this->cookies->set('email',$email);
                        $this->cookies->set('password',$user->password);
                    }

                    $this->session->set('userId',$user->userId);

                    print json_encode(array('status' => 'success', 'isMessage' => false, 'message' => '', 'url' => '/user/profile'));

                }else{
                    print json_encode(array('status' => 'fail', 'messages' => array($this->translate->query("Password is wrong"))));
                }
            }

        }

        if($this->request->isAjax()){
            $this->view->disable();
            $this->response->setContentType('application/json', 'UTF-8');
        }

    }

    public function forgotPasswordAction(){
        Phalcon\Tag::setTitle($this->translate->query("Password recovery"));
        $this->view->setVar('isDarkackgroud',true);

        if($this->request->isPost()){
            $user = User::findFirst(array(
                        'conditions' => 'email=?1',
                        'bind' => array(
                            1 => $this->request->getPost('email','email')
                        )
                    ));
            if(!$user){
                $this->flashSession->error($this->translate->query("Can not find e-mail"));
            }else{
                $userRecovery = new UserRecover();
                $userRecovery->userId = $user->userId;
                $userRecovery->hashCode = $this->_user->generateRandomString(7).time();
                $userRecovery->hashDate = time();
                if(!$userRecovery->save()){
                    $this->flashSession->error($this->translate->query("Ooops! Something went wrong"));
                }else{

                    try {
                        $this->notify->sendEmail($user->email, $this->translate->query("Password_recovery_mail_title"), array('user','passwordRecovery'), array(), array('userObj'=>$user,'hash'=>$userRecovery->hashCode));
                    }catch(\Swift_TransportException $e) {
                        //
                    }

                    $this->dispatcher->forward(array(
                        'controller' => 'user',
                        'action' => 'passwordIsSent'
                    ));
                    return;
                }
            }
        }

    }


    public function passwordIsSentAction(){
        Phalcon\Tag::setTitle($this->translate->query("Password recovery email is sent"));
    }

    function recoverPasswordAction($hash = ""){
        $this->view->setVar('isDarkackgroud',true);

        $hashObj = UserRecover::findFirst(array(

            "conditions" => "hashCode = :hash: AND isUsed = 0",
            "bind"       => array('hash' => $hash),
            "order"      => "hashDate DESC",
            "limit"      => 1
        ));

        $userHash = UserRecover::findFirst(array(
            "conditions" => "userId = :userId: AND isUsed = 0",
            "bind"       => array("userId" => $hashObj->userId),
            "order"      => "hashDate DESC"
        ));

        if($hashObj->hashCode == $userHash->hashCode){
            $this->session->set('userId',$userHash->userId);
            $this->dispatcher->forward(array(
                'controller' => 'user',
                'action' => 'setNewPassword'
            ));
            return;
        }

    }

    function setNewPasswordAction(){

        $this->view->setVar('isDarkackgroud',true);


        if($this->request->isPost() && $this->session->has('userId')){


            $password1 = $this->request->getPost('password1');
            $password2 = $this->request->getPost('password2');

            $userDetails = User::findFirst(
                array(
                    'conditions' => "userId=?1",
                    'bind' => array(1 => $this->session->get('userId'))
                )
            );

            $wasError = false;

            if(!$userDetails){
                $wasError = true;
                $errorsArray['userNotFound'] = $this->translate->query("User not found");
            }else{

            }




            if (($password1 == $password2) && (mb_strlen($password1) >= 6)) {

                $userDetails->password = $this->security->hash($this->request->getPost('password1'));

            } else {
                $wasError = true;
                if (mb_strlen($password1) < 6) {
                    $wasError = true;
                    $errorsArray['password1'] = $this->translate->query("Password have to be 6 or more chars");
                } else {
                    $wasError = true;
                    $errorsArray['password1'] = $this->translate->query("Passwords do not match");
                }
            }
            foreach ($errorsArray as $message) {
                $this->flashSession->error($message);
            }
            //@Todo: catch errors if save() fails. display error/success messages for ajax/html response
            if(!$wasError){

                if(!$userDetails->save()){
                    $wasError = true;
                    $errorsArray = "";
                    foreach ($userDetails->getMessages() as $message) {
                        $errorsArray[] = $message;
                    }
                    if($this->request->isAjax()){
                        print json_encode(array('status'=>'fail','messages'=>$errorsArray));
                    }else {
                        foreach ($errorsArray as $errorFromArray){
                            $this->flashSession->error($errorFromArray);
                        }
                    }

                }else{



                    //success
                    if($this->request->isAjax()) {
                        print json_encode(array('status' => 'success', 'isMessage' => false, 'message' => '', 'url' => '/user/profile'));
                    }else{
                        $this->response->redirect("/user/profile");
                    }
                }
            }else{
                if($this->request->isAjax()) {
                    print json_encode(array('status'=>'fail','messages'=>$errorsArray));
                }else{

                    foreach ($errorsArray as $message) {
                        $this->flashSession->error($message);
                    }
                }
            }

        }


        if($this->request->isAjax()){
            $this->view->disable();
            $this->response->setContentType('application/json', 'UTF-8');
        }
    }

    public function logoutAction(){
        Phalcon\Tag::setTitle($this->translate->query("You have logged out"));
        $this->session->remove('userId');
        $this->cookies->get('rememberMe')->delete();
        $this->cookies->get('password')->delete();
        $this->cookies->get('email')->delete();
    }

    public function profileAction(){
        Phalcon\Tag::setTitle($this->translate->query("Profile and overview"));
    }
}

