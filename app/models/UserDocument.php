<?php

class UserDocument extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     * @Primary
     * @Identity
     * @Column(type="integer", length=11, nullable=false)
     */
    public $documentId;

    /**
     *
     * @var integer
     * @Column(type="integer", length=11, nullable=true)
     */
    public $userId;

    /**
     *
     * @var integer
     * @Column(type="integer", length=11, nullable=true)
     */
    public $documentType;

    /**
     *
     * @var string
     * @Column(type="string", nullable=true)
     */
    public $documentName;

    /**
     *
     * @var integer
     * @Column(type="integer", length=4, nullable=true)
     */
    public $documentStatus;

    /**
     *
     * @var string
     * @Column(type="string", nullable=true)
     */
    public $documentPath;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->setSchema("paxful");
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'user_document';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return UserDocument[]|UserDocument
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return UserDocument
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
