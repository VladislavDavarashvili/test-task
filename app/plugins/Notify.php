<?php

use Phalcon\Mvc\View\Engine\Volt as VoltEngine,
    Phalcon\Mvc\View,
    Phalcon\Mvc\User\Plugin;

class Notify extends Plugin
{

    public $lang;
    public $dbname;

    private $mail_conf;
    private $sms_conf;
    private $debugDir;
    private $view;
    private $translate;

    protected $_transport;

    function translate($string){
        return $string;
    }

    function __construct()
    {

        require_once (realpath(__DIR__).'/Swift/swift_required.php');


        $this->view = new View();
        $this->view->setViewsDir('../app/views/email_views/');

        $this->view->setDI($this->getDI());

        $this->view->registerEngines(array(
            '.volt' => function ($view, $di) {
                $volt = new VoltEngine($view, $di);
                $volt->setOptions(array(
                    'compiledPath' => '../app/cache/',
                    'compiledSeparator' => '_'
                ));
                $compiler = $volt->getCompiler();
                $volt->getCompiler()->addFunction('is_a', 'is_a');
                $volt->getCompiler()->addFunction('substr', 'substr');

                $volt->getCompiler()->addFunction('_', function($resolvedArgs, $exprArgs){
                    return "'".$this->translate($exprArgs[0]['expr']['value'])."'";
                });
                $volt->getCompiler()->addFunction('_Lang', function($resolvedArgs, $exprArgs){
                    return "'".$this->translate($exprArgs[0]['expr']['value'],$exprArgs[1]['expr']['value'])."'";
                });


                return $volt;
            }
        ));
        $conf = $this->getDI()->get('config');
        $this->mail_conf = $conf->mail;
        $this->sms_conf = $conf->sms;
        $this->debugDir = $conf->application->debugDir;


    }



    public function sendSmsNotification($to, $message, $isHigh = 0)
    {
        if (!$to || !$message){
            return false;
        }

        if (mb_strlen($message) > 402){
            return false;
        }

        $parameters = array_merge(array('to' => $to, 'message' => $message), (array)$this->sms_conf);
        if($isHigh == 1){
            $parameters = array_merge(array('fast'=>1), $parameters);
        }

        $data = '?'.http_build_query($parameters);
        $file = fopen('https://api.smsapi.com/sms.do'.$data, 'r');
        $result = fread($file, 1024);

        return strpos($result, 'OK');
    }

    /**
     * Applies a template to be used in the e-mail
     *
     * @param string $name
     * @param array $params
     */
    public function getTemplate($name, $params)
    {
        if (!is_array($params)){
            $params = array();
        }
        $parameters = array_merge(array('publicUrl' => $this->url->getBaseUri()), $params);
        list($folder, $template) = $name;
        $this->view->setRenderLevel(View::LEVEL_LAYOUT);
        return $this->view->getRender($folder, $template, $parameters);
    }

    /**
     * Sends e-mails based on predefined templates
     *
     * @param array $to
     * @param string $subject
     * @param string $name
     * @param array $params
     */
    function sendEmail($to, $subject, $name, $from = array(), $params)
    {
        $templateX = $this->getTemplate($name, $params);
        //echo $templateX;
        list($folder, $template) = $name;

        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
        $plain_template = $this->view->getRender($folder,$template,$params);

        $from = array(
            $this->mail_conf->fromEmail => $this->mail_conf->fromName
        );

        $message = Swift_Message::newInstance()
            ->setSubject($subject)
            ->setTo($to)
            ->setFrom($from)
            ->setBody(strip_tags($plain_template))
            ->addPart($templateX, 'text/html');

        //echo 'text:'.$template.' html:'.$templateX;

        if (!$this->_transport) {
            $this->_transport = Swift_SmtpTransport::newInstance(
                $this->mail_conf->smtp->server,
                $this->mail_conf->smtp->port,
                $this->mail_conf->smtp->security
            )->setUsername($this->mail_conf->smtp->username)->setPassword($this->mail_conf->smtp->password);
        }

        $mailer = Swift_Mailer::newInstance($this->_transport);
        return $mailer->send($message);
    }

    public function setMailConf($mail_configuration)
    {
        $this->mail_conf = $mail_configuration;
    }

    public function setSmsConf($sms_configuration)
    {
        $this->sms_conf = $sms_configuration;
    }

    function sendDebug($params)
    {
        $templateX = $this->getTemplate(array("developer","UserController"), $params);

        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
        $plain_template = $this->view->getRender("developer","UserController",$params);


        $fileName = $this->debugDir.''.date("d")."-".date("m")."-".date("Y")." G:i:s.txt";
        $file = fopen($fileName,"w");
        fwrite($file,$params['message']);
        fclose($file);


        $from = array(
            $this->mail_conf->fromEmail => $this->mail_conf->fromName
        );

        $message = Swift_Message::newInstance()
            ->setSubject("Problem on website")
            ->setTo($this->mail_conf->developerMail)
            ->setFrom($from)
            ->setBody(strip_tags($plain_template))
            ->addPart($templateX, 'text/html');

        if (!$this->_transport) {
            $this->_transport = Swift_SmtpTransport::newInstance(
                $this->mail_conf->smtp->server,
                $this->mail_conf->smtp->port,
                $this->mail_conf->smtp->security
            )->setUsername($this->mail_conf->smtp->username)->setPassword($this->mail_conf->smtp->password);
        }

        $mailer = Swift_Mailer::newInstance($this->_transport);
        return $mailer->send($message);
    }
}
