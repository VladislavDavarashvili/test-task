<?php
/**
 * Created by PhpStorm.
 * User: vladislav
 * Date: 12/06/14
 * Time: 14:05
 */
use Phalcon\Events\Event,
   Phalcon\Mvc\User\Plugin,
   Phalcon\Mvc\Dispatcher,
   Phalcon\Acl;


class Security extends Plugin {
       public function __construct($dependencyInjector) {
           $this->_dependencyInjector = $dependencyInjector;
       }

       public function getAcl() {
           unset($this->persistent->acl);
           unset($_SESSION['Security']['acl']);

           if (!isset($this->persistent->acl)) {
               $acl = new Phalcon\Acl\Adapter\Memory();

               $acl->setDefaultAction(Phalcon\Acl::DENY);
               $roles = array(
                   'admin' => new Phalcon\Acl\Role('Admin'),
                   'users' => new Phalcon\Acl\Role('Users'),
                   'guests' => new Phalcon\Acl\Role('Guests')
               );
               foreach ($roles as $role) {
                   $acl->addRole($role);
               }


               $adminControllers = array(
                 'admin'=>'*',
                 'media' => '*'
               );
               foreach ($adminControllers as $resource => $actions) {
                   $acl->addResource(new Phalcon\Acl\Resource($resource), $actions);
               }

               $userContollers = array(
                 'user' => array('profile','logout','index','setNewPassword','passwordIsSent'),
               );
               foreach ($userContollers as $resource => $actions) {
                   $acl->addResource(new Phalcon\Acl\Resource($resource), $actions);
               }

               $publicControllers = array(
                   'index' => '*',
                   'error' => '*',
                   'user' => array('recoverPassword')
               );
               foreach ($publicControllers as $resource => $actions) {
                   $acl->addResource(new Phalcon\Acl\Resource($resource), $actions);
               }

               $guestsOnlyControllers = array(
                   'user' => array('login','register','forgotPassword','passwordIsSent','setNewPassword'),
                   );

               foreach($guestsOnlyControllers as $resource => $actions){
                   $acl->addResource(new Phalcon\Acl\Resource($resource), $actions);
               }

               /* add all public things to every profile with no 'action limit' (*) */
               foreach ($roles as $role) {
                   foreach ($publicControllers as $resource => $actions) {
                    if($actions == '*'){
                        $acl->allow($role->getName(), $resource, '*');
                    }else {
                        foreach ($actions as $action) {
                            $acl->allow($role->getName(), $resource, $action);
                        }
                    }
                   }
               }



               foreach($guestsOnlyControllers as $resource => $actions){
                   if($actions == '*'){
                       $acl->allow('Guests',$resource,'*');
                   }else{
                       foreach($actions as $action){
                           $acl->allow('Guests',$resource,$action);
                       }
                   }
               }



               foreach($adminControllers as $resource=>$action){
                   $acl->allow('Admin',$resource,'*');
               }
               foreach($userContollers as $resource=>$action){
                   $acl->allow('Admin',$resource,'*');
               }
               foreach($guestsOnlyControllers as $resource=>$action){
                   $acl->allow('Admin',$resource,'*');
               }

               foreach($userContollers as $resource=>$actions){
                   if($actions == '*'){
                       $acl->allow('Users',$resource,'*');
                   }else{
                       foreach($actions as $action){
                           $acl->allow('Users',$resource,$action);
                       }
                   }
               }


               $this->persistent->acl = $acl;
           }
           return $this->persistent->acl;
       }

       public function beforeExecuteRoute(Event $event, Dispatcher $dispatcher) {


           $user = $this->session->get('userId');

           if (!$user) {
               $role = 'Guests';
           } else {
               $role = 'Users';


               if($this->session->get('isAdmin')=='1'){
                   $role = 'Admin';
               }

               if($this->session->get('isSuperAdmin')=='1' && $this->session->get('isAdmin') == 1){
                   $role = 'SuperAdmin';
               }


           }



           $controller = $dispatcher->getControllerName();
           $action = $dispatcher->getActionName();
           $acl = $this->getAcl();



                $allowed = $acl->isAllowed($role, $controller, $action);
                if ($allowed != Acl::ALLOW) {

                    //if($controller)
                    $dispatcher->forward(
                        array(
                            'controller' => 'user',
                            'action' => 'login'
                        ));
                    return false;
                }

       }

       public function beforeException(Event $event, Dispatcher $dispatcher,  $exception){

       if($exception->getCode() == 0 ){
           /*
           echo "file: ".$exception->getFile().'<BR>';
           echo "line: ".$exception->getLine().'<BR>';
           echo "trace:".$exception->getTraceAsString().'<BR>';

           die("beforeExepction ".$dispatcher->getControllerName().' - '.$exception->getMessage().' - code:'.$exception->getCode());
           */

       }
        //
           switch ($exception->getCode()) {
               case 0:

                   $dispatcher->forward(array(
                       'controller' => 'error',
                       'action' => 'pageIsNotResponding',
                       'params' => array('exception' => $exception, 'controllerName'=>$dispatcher->getControllerName(), 'actionName' => $dispatcher->getActionName())
                   ));

                   return $event->isStopped();
               break;
               case \Phalcon\Dispatcher::EXCEPTION_HANDLER_NOT_FOUND:
               case \Phalcon\Dispatcher::EXCEPTION_INVALID_HANDLER:
               case \Phalcon\Dispatcher::EXCEPTION_ACTION_NOT_FOUND:
               case \Phalcon\Dispatcher::EXCEPTION_INVALID_PARAMS:


                        $dispatcher->forward(array(
                            'controller' => 'error',
                            'action' => 'pageNotFound',
                            'params' => array('exception' => $exception)
                        ));



                   return $event->isStopped();
               break;
               default:

               break;
           }
       }
}