<?php

/**
 * Created by PhpStorm.
 * User: vladislavdavarasvili
 * Date: 18/06/2017
 * Time: 13:38
 */
class UserFunctions extends \Phalcon\Mvc\User\Plugin
{
    function generateRandomString($length = 15) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

}