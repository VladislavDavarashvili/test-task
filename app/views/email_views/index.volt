<!DOCTYPE HTML>
<html>
<head>
    <meta charset="utf-8">
    <meta name="format-detection" content="telephone=no">
    <title>{{ get_title() }}</title>
</head>

<body style="width:100%;background: #fff;margin:0;padding:0;font-family: Arial,sans-serif;font-size:14px;font-weight:normal;color:#232323;line-height: 1.2;">

<table cellpadding="0" cellspacing="0" style="width:100%;margin:0;padding:10px;" >
    <tr>
        <td>
            <table style="width:100%; margin:0 auto 0 auto;border-spacing: 0;border-collapse: collapse;">
                <thead>
                <tr>
                    <th style="text-align: left;border-bottom:1px solid #d8d8d8;">
                       Logo
                    </th>

                </tr>
                </thead>
                <tr>
                    <td colspan="2" style="border-bottom:1px solid #d8d8d8;">

                        {{ content() }}
                    </td>
                </tr>

                <tr>
                    <td>
                        {{ _("With the best wishes, Trully yours team") }}
                        <a href="mailto:support@paxful.com">support@paxful.com</a>
                    </td>
                    <td>

                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>


</body>
</html>