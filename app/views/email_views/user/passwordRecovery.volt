{{ _("Dear") }} {{ userObj.name }}{% if userObj.middleName != "" %}{{ userObj.middleName }}{% endif %},
<h4>{{ _("Password recovery link is below") }}</h4>
{{ _("You can recover password clicking confirmation link below or copying it to browser") }}<BR>
<a href="{{ config.application.publicUrl }}user/recoverPassword/{{ hash }}" >{{ config.application.publicUrl }}user/recoverPassword/{{ hash }}</a>
<BR>
{{ _("If you was not recovering password, just ignore this e-mail") }}

<BR><BR>
