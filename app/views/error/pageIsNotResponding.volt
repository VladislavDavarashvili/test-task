<div class="row m-t-100">
    <div class="col-sm-12">
        <h1 class="text-center super-huge-error m-t-100"><i class="ti-direction-alt"></i></h1>
        <h3 class="text-center">{{ _("Oops! Page is not responding") }}</h3>

        <div class="well m-t-25">
            {{ outputMessage }}
        </div>
    </div>
</div>