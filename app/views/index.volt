<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        {{ get_title() }}
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <link rel="stylesheet" href="/css/style.css">
        <link rel="stylesheet" href="/css/style.css">
        <link rel="stylesheet" href="/css/themify-icons.css">
        <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,600,700&amp;subset=cyrillic" rel="stylesheet">

        <meta name="viewport" content="width=device-width, initial-scale=0.999999" />

        <!-- Server side assign -->
        {% if headerCss is defined %}
            {% for cssScript in headerCss %}
                <link rel="stylesheet" href="{{ cssScript }}" type="text/css" />
            {% endfor %}
        {% endif %}
        <!--/Server side assign -->
    </head>
    <body class="stretched {% if isDarkackgroud %}darkBackground{% endif %}"  data-loader-color="#258ad5" data-loader-timeout="1500">
        <div id="wrapper" style="opacity: 1">
            <header id="header">
                <div id="header-wrap">
                    <div class="container sm-full-width xs-full-width clearfix">
                        <div class="pull-left visible-xs">
                            <div class="header-inner">
                                <div id="menu-trigger"><i class="fa fa-reorder"></i></div>
                            </div>
                        </div>
                        <div class="pull-left">
                            <div id="logo" class="header-inner">
                                <a href="/">
                                    <img src="/img/logo@2x.png" alt="PaxFul" data-src="/img/logo.png" data-src-retina="/img/logo@2x.png" width="150">
                                </a>
                            </div>
                        </div>
                        <nav class="pull-left main-menu hidden-xs">
                            <ul>
                                <li><a href="/invest"><div>Investment opportunities</div></a></li>
                                <li><a href="/borrow-money"><div>Borrow money</div></a></li>
                            </ul>
                        </nav>


                        {% if userObj is defined %}
                        <div class="pull-right m-t-30">
                            <a href="/user"><i class="ti-user"></i> Hey {{ userObj.firstname }}</a>,<a class="m-l-20" href="/user/logout">{{ _("Logout") }} <i class="ti-close"></i></a>
                        </div>
                        {% else %}
                        <div class="pull-right">
                            <nav class="nav-secondary header-inner">
                                <ul>
                                    <li><a href="/user/login"><i class="ti-user"></i> {{ _("Log in") }}</a></li>
                                    <li><a href="/user/register"><i class="ti-target"></i> {{ _("Sign up") }}</a></li>
                                </ul>
                            </nav>
                        </div>
                        {% endif %}


                    </div>
                    <div id="header-popup-wrapper">
                        {% if flashSession is defined %}
                            {% for type, messages in flashSession.getMessages() %}
                                <div class="box-body">
                                    {% for message in messages %}
                                       <div class="header-popup {{ type }}"><div class="header-popup-container">
                                            {{ message }}
                                            <i class="close-header-popup ti-close"></i></div></div>
                                    {% endfor %}
                                </div>
                            {% endfor %}
                        {% endif %}

                    </div>
                </div>

            </header>

            <section id="content ">

                <div class="content-wrap">
                    <div class="container">
                        {{ content() }}

                    </div>
                </div>
            </section>



        </div>
        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
        <!-- Latest compiled and minified JavaScript -->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>


        <script type="text/javascript" src="/js/plugins/loader.js"></script>
        <script type="text/javascript" src="/js/functions.js"></script>

        <!-- Server side assign -->
        {% if footerJs is defined %}
            {% for jsScript in footerJs %}
                <script type="text/javascript" src="{{ jsScript }}"></script>
            {% endfor %}
        {% endif %}
        <!--/Server side assign -->
    </body>


</html>
