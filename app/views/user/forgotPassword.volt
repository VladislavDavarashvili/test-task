<div class="row m-t-25">
    <div class="col-sm-4 col-sm-push-4 white-background cornerRadius5 p-b-15 p-r-25 p-l-25">
        <form method="post" action="/user/forgotPassword" {# class="dynoForm" #} >
            <div class="row m-t-15">
                <div class="col-sm-12">
                    <h2>{{ _("Recover password") }}</h2>
                </div>
            </div>


            <div class="row m-t-15">
                <div class="col-sm-12">
                    <div class="form-group">
                        <label for="email">{{ _("Email") }}</label>
                        <input type="text" name="email" id="email" class="form-control" >
                    </div>
                </div>
            </div>

            <div class="row m-t-15">
                <div class="col-sm-12">
                    <button class="btn btn--primary pull-right" id="recover" name="recover">{{ _("Recover password") }}</button>
                </div>
            </div>
            <div class="row m-t-15">
                <div class="col-sm-12">
                    <a href="/user/login"><i class="ti-arrow-left"></i> {{ _("Back to login screen") }}</a>
                </div>
            </div>



        </form>

    </div>
</div>