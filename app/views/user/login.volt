<div class="row m-t-25">
    <div class="col-sm-4 col-sm-push-4 white-background cornerRadius5 p-b-15 p-r-25 p-l-25">
        <form method="post" action="/user/login" class="dynoForm" >
            <div class="row m-t-15">
                <div class="col-sm-12">
                    <h2>{{ _("Login") }}</h2>
                </div>
            </div>


            <div class="row m-t-15">
                <div class="col-sm-12 col-md-6">
                    <div class="form-group">
                        <label for="email">{{ _("Email") }}</label>
                        <input type="text" name="email" id="email" class="form-control" >
                    </div>
                </div>

                <div class="col-sm-12 col-md-6 sm-m-t-15">
                    <div class="form-group">
                        <label for="password">{{ _("Password") }}</label>
                        <input type="password" name="password" id="password" class="form-control" >
                    </div>
                </div>

            </div>

            <div class="row m-t-15">
                <div class="col-sm-12">
                    <label for="rememberMe">
                        <input type="checkbox" name="rememberMe" id="rememberMe" value="1"> {{ _("Remember me") }}
                    </label>
                </div>
            </div>
            <div class="row m-t-15">
                <div class="col-sm-12">
                    <button class="btn btn--primary pull-right" id="login" name="login">{{ _("Login") }}</button>
                </div>
            </div>
            <div class="row m-t-15">
                <div class="col-sm-12">
                    <a href="/user/forgotPassword">{{ _("Forgot password?") }}</a>
                </div>
            </div>



        </form>

    </div>
</div>