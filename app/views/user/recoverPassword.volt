<div class="row m-t-25">
    <div class="col-sm-6 col-sm-push-3 white-background cornerRadius5 p-b-15 p-r-25 p-l-25">


            <h3 class="colorError">{{ _("Password recovery link is expired") }}</h3>
            {{ _("password_recovery_expired_promo_text") }}
            <div class="clearfix">&nbsp;</div>

    </div>
</div>