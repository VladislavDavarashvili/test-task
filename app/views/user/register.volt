<div class="row m-t-25">
    <div class="col-sm-4 col-sm-push-4 white-background cornerRadius5 p-b-15 p-r-25 p-l-25">
        <form method="post" action="/user/register" class="dynoForm" >
            <div class="row m-t-15">
                <div class="col-sm-12">
                    <h2>{{ _("Sign up") }}</h2>
                </div>
            </div>
            <div class="row m-t-15">
                <div class="col-sm-12">
                    <div class="form-group">
                        <label for="firstname">{{ _("Name") }}</label>
                        <input type="text" name="firstname" id="firstname" class="form-control" >
                    </div>
                </div>
            </div>
            <div class="row m-t-15">
                <div class="col-sm-12">
                    <div class="form-group">
                        <label for="lastname">{{ _("Lastname") }}</label>
                        <input type="text" name="lastname" id="lastname" class="form-control" >
                    </div>
                </div>
            </div>

            <div class="row m-t-15">
                <div class="col-sm-12">
                    <div class="form-group">
                        <label for="email">{{ _("Email") }}</label>
                        <input type="text" name="email" id="email" class="form-control" >
                    </div>
                </div>
            </div>

            <div class="row m-t-15">
                <div class="col-sm-12">
                    <div class="form-group">
                        <label for="password">{{ _("Password") }}</label>
                        <input type="password" name="password" id="password" class="form-control" >
                    </div>
                </div>
                <div class="col-sm-12">
                    <div class="form-group">
                        <label for="password2">{{ _("Password once again") }}</label>
                        <input type="password" name="password2" id="password2" class="form-control" >
                    </div>
                </div>
            </div>

            <div class="row m-t-15">
                <div class="col-sm-12">
                    <label for="iConfirm">
                        <input type="checkbox" name="iConfirm" id="iConfirm" value="1"> {{ _("I confirm terms&conditions") }}
                    </label>
                </div>
            </div>
            <div class="row m-t-15">
                <div class="col-sm-12">
                    <button class="btn btn--primary pull-right" id="register" name="register">{{ _("Register") }}</button>
                </div>
            </div>


        </form>

    </div>
</div>