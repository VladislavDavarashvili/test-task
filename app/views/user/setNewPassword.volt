<div class="row m-t-25">
    <div class="col-sm-6 col-sm-push-3 white-background cornerRadius5 p-b-15 p-r-25 p-l-25">


                <h3>{{ _("Create new password") }}</h3>
                {{ _("create_new_password_promo_text") }}
                <div class="clearfix">&nbsp;</div>
                <form method="post" action="/user/setNewPassword" id="setNewPassword">
                <div class="col-sm-6 noleftpadding">
                    <div class="form-group required">
                        <label>{{ _("Password") }}<small></small></label>
                        <input type="password" name="password1" id="password1" required class="form-control" >
                    </div>
                </div>
                <div class="col-sm-6 noleftpadding">
                    <div class="form-group required">
                        <label>{{ _("Re-type Password") }} <small>{{ _("Once again") }}</small></label>
                        <input type="password" name="password2" id="password2" required class="form-control" >
                    </div>
                </div>

                    <div class="clearfix"></div>
                <div class="col-sm-12">
                    <button class="btn btn--primary pull-right">{{ _("Set new password") }} <i class="ti-angle-double-right"></i></button>
                </div>
                </form>


    </div>
</div>