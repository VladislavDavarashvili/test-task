CREATE TABLE `user` (
  `userId` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `email` text,
  `password` text,
  `firstname` text,
  `lastname` text,
  `language` text,
  `createDate` int(11) DEFAULT NULL,
  `isVerified` tinyint(1) DEFAULT '0',
  `emailHash` text,
  PRIMARY KEY (`userId`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

CREATE TABLE `user_document` (
  `documentId` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `userId` int(11) DEFAULT NULL,
  `documentType` int(11) DEFAULT NULL,
  `documentName` text,
  `documentStatus` tinyint(4) DEFAULT NULL,
  `documentPath` text,
  PRIMARY KEY (`documentId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE `user_recover` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `userId` int(11) DEFAULT NULL,
  `hashCode` text,
  `hashDate` int(11) DEFAULT NULL,
  `isUsed` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `userId` (`userId`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;