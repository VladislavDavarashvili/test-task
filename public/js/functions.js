/**
 * Created by vladislavdavarasvili on 17/06/2017.
 */

(function() {
    var lastTime = 0;
    var vendors = ['ms', 'moz', 'webkit', 'o'];
    for(var x = 0; x < vendors.length && !window.requestAnimationFrame; ++x) {
        window.requestAnimationFrame = window[vendors[x]+'RequestAnimationFrame'];
        window.cancelAnimationFrame = window[vendors[x]+'CancelAnimationFrame']
            || window[vendors[x]+'CancelRequestAnimationFrame'];
    }

    if (!window.requestAnimationFrame)
        window.requestAnimationFrame = function(callback, element) {
            var currTime = new Date().getTime();
            var timeToCall = Math.max(0, 16 - (currTime - lastTime));
            var id = window.setTimeout(function() { callback(currTime + timeToCall); },
                timeToCall);
            lastTime = currTime + timeToCall;
            return id;
        };

    if (!window.cancelAnimationFrame)
        window.cancelAnimationFrame = function(id) {
            clearTimeout(id);
        };
}());

$(document).ready(function () {

    /** Hide from html defined alerts **/
    /*
    setTimeout(function(){
        $('.header-popup').animate({
            opacity:0,
        },300,function(){
            $(this).animate({
                height:0
            },300,function () {
                $(this).remove()
            })
        });
    },3000);*/


    var $dynoForm = $('form.dynoForm');
    $dynoForm.submit(function(e) {
        e.preventDefault();
        // validation

        // start loading animation
        loading_start();

        $('input',$(this)).removeClass('error');
        $('label',$(this)).removeClass('error');

        // get data from form
        var values = $(this).serialize();

        // send ajax request
        $.ajax({
            url: $(this).attr('action'),
            type: 'POST',
            data: values,
            dataType: 'json',
            success: function(result) {
                // pixel
                if(result.status == "fail"){
                    loading_stop();
                    console.log("dynoform");
                    if(Array.isArray(result.messages) || Object.keys(result.messages).length > 0){
                        console.log("dino aray");
                        if(Object.keys(result.messages).length > 0){
                            $.each(result.messages,function(key,val){
                                animateOpenHeaderPopup('error',val);
                                $('#'+key).addClass('error');

                            });
                        }
                    }else{
                        animateOpenHeaderPopup('error',result.messages);
                    }

                }else{
                    if(result.isMessage == true){
                        loading_stop();
                        animateOpenHeaderPopup('success',result.message);
                    }else{
                        if(result.url != ""){
                            console.log("redirect to "+result.url);
                            window.location = result.url;
                        }
                    }
                }

            },
            fail: function() {

                animateOpenHeaderPopup('error',checkInternet);

            }
        });
    });

    $('input',$dynoForm).focus(function(){
        $(this).removeClass('error');
    });


    function animateCloseHeaderPopup($el) {

        $('.header-popup-container',$el).animate({
            opacity:0,
        },300,function(){
            $el.animate({
                height:0
            },300,function () {
                $el.remove()
            })
        });
    }


    function animateOpenHeaderPopup(popupType,popupText) {
        var headerPopup = '<div class="header-popup '+popupType+'"><div class="header-popup-container">';
        headerPopup+= popupText;
        headerPopup+= '<i class="close-header-popup ti-close"></i></div></div>';
        var $headerPopup = $(headerPopup)
        $('#header-popup-wrapper').append($headerPopup);

        setTimeout(function(){

            animateCloseHeaderPopup($headerPopup)
        },3000)
    }

    $(document).on('click','.close-header-popup.ti-close',function(){
        animateCloseHeaderPopup($(this).parent().parent());
    });

    function loading_start(){
        $("#wrapper").append('<div class="loaderOverlay"><div class="loader">Loading...</div></div>');
    }
    function loading_stop(){
        $('#wrapper .loaderOverlay').remove();
    }

    /*
    On document ready events
     */
     var $wrapper = $('#wrapper'),
         $header = $('#header'),
         $headerWrap = $('#header-wrap'),
         $content = $('#content'),
         $footer = $('#footer'),
         $body = $('body');

        if( $body.hasClass('no-transition') ) { return true; }

        if( !$().animsition ) {
            $body.addClass('no-transition');
            console.log('pageTransition: Animsition not Defined.');
            return true;
        }

        window.onpageshow = function(event) {
            if(event.persisted) {
                window.location.reload();
            }
        };

        var animationIn = $body.attr('data-animation-in'),
            animationOut = $body.attr('data-animation-out'),
            durationIn = $body.attr('data-speed-in'),
            durationOut = $body.attr('data-speed-out'),
            loaderTimeOut = $body.attr('data-loader-timeout'),
            loaderStyle = $body.attr('data-loader'),
            loaderColor = $body.attr('data-loader-color'),
            loaderStyleHtml = $body.attr('data-loader-html'),
            loaderBgStyle = '',
            loaderBorderStyle = '',
            loaderBgClass = '',
            loaderBorderClass = '',
            loaderBgClass2 = '',
            loaderBorderClass2 = '';

        if( !animationIn ) { animationIn = 'fadeIn'; }
        if( !animationOut ) { animationOut = 'fadeOut'; }
        if( !durationIn ) { durationIn = 1500; }
        if( !durationOut ) { durationOut = 800; }
        if( !loaderStyleHtml ) { loaderStyleHtml = '<div class="css3-spinner-bounce1"></div><div class="css3-spinner-bounce2"></div><div class="css3-spinner-bounce3"></div>'; }

        if( !loaderTimeOut ) {
            loaderTimeOut = false;
        } else {
            loaderTimeOut = Number(loaderTimeOut);
        }

        if( loaderColor ) {
            if( loaderColor == 'theme' ) {
                loaderBgClass = ' bgcolor';
                loaderBorderClass = ' border-color';
                loaderBgClass2 = ' class="bgcolor"';
                loaderBorderClass2 = ' class="border-color"';
            } else {
                loaderBgStyle = ' style="background-color:'+ loaderColor +';"';
                loaderBorderStyle = ' style="border-color:'+ loaderColor +';"';
            }
            loaderStyleHtml = '<div class="css3-spinner-bounce1'+ loaderBgClass +'"'+ loaderBgStyle +'></div><div class="css3-spinner-bounce2'+ loaderBgClass +'"'+ loaderBgStyle +'></div><div class="css3-spinner-bounce3'+ loaderBgClass +'"'+ loaderBgStyle +'></div>'
        }

        if( loaderStyle == '2' ) {
            loaderStyleHtml = '<div class="css3-spinner-flipper'+ loaderBgClass +'"'+ loaderBgStyle +'></div>';
        } else if( loaderStyle == '3' ) {
            loaderStyleHtml = '<div class="css3-spinner-double-bounce1'+ loaderBgClass +'"'+ loaderBgStyle +'></div><div class="css3-spinner-double-bounce2'+ loaderBgClass +'"'+ loaderBgStyle +'></div>';
        } else if( loaderStyle == '4' ) {
            loaderStyleHtml = '<div class="css3-spinner-rect1'+ loaderBgClass +'"'+ loaderBgStyle +'></div><div class="css3-spinner-rect2'+ loaderBgClass +'"'+ loaderBgStyle +'></div><div class="css3-spinner-rect3'+ loaderBgClass +'"'+ loaderBgStyle +'></div><div class="css3-spinner-rect4'+ loaderBgClass +'"'+ loaderBgStyle +'></div><div class="css3-spinner-rect5'+ loaderBgClass +'"'+ loaderBgStyle +'></div>';
        } else if( loaderStyle == '5' ) {
            loaderStyleHtml = '<div class="css3-spinner-cube1'+ loaderBgClass +'"'+ loaderBgStyle +'></div><div class="css3-spinner-cube2'+ loaderBgClass +'"'+ loaderBgStyle +'></div>';
        } else if( loaderStyle == '6' ) {
            loaderStyleHtml = '<div class="css3-spinner-scaler'+ loaderBgClass +'"'+ loaderBgStyle +'></div>';
        } else if( loaderStyle == '7' ) {
            loaderStyleHtml = '<div class="css3-spinner-grid-pulse"><div'+ loaderBgClass2 + loaderBgStyle +'></div><div'+ loaderBgClass2 + loaderBgStyle +'></div><div'+ loaderBgClass2 + loaderBgStyle +'></div><div'+ loaderBgClass2 + loaderBgStyle +'></div><div'+ loaderBgClass2 + loaderBgStyle +'></div><div'+ loaderBgClass2 + loaderBgStyle +'></div><div'+ loaderBgClass2 + loaderBgStyle +'></div><div'+ loaderBgClass2 + loaderBgStyle +'></div><div'+ loaderBgClass2 + loaderBgStyle +'></div></div>';
        } else if( loaderStyle == '8' ) {
            loaderStyleHtml = '<div class="css3-spinner-clip-rotate"><div'+ loaderBorderClass2 + loaderBorderStyle +'></div></div>';
        } else if( loaderStyle == '9' ) {
            loaderStyleHtml = '<div class="css3-spinner-ball-rotate"><div'+ loaderBgClass2 + loaderBgStyle +'></div><div'+ loaderBgClass2 + loaderBgStyle +'></div><div'+ loaderBgClass2 + loaderBgStyle +'></div></div>';
        } else if( loaderStyle == '10' ) {
            loaderStyleHtml = '<div class="css3-spinner-zig-zag"><div'+ loaderBgClass2 + loaderBgStyle +'></div><div'+ loaderBgClass2 + loaderBgStyle +'></div></div>';
        } else if( loaderStyle == '11' ) {
            loaderStyleHtml = '<div class="css3-spinner-triangle-path"><div'+ loaderBgClass2 + loaderBgStyle +'></div><div'+ loaderBgClass2 + loaderBgStyle +'></div><div'+ loaderBgClass2 + loaderBgStyle +'></div></div>';
        } else if( loaderStyle == '12' ) {
            loaderStyleHtml = '<div class="css3-spinner-ball-scale-multiple"><div'+ loaderBgClass2 + loaderBgStyle +'></div><div'+ loaderBgClass2 + loaderBgStyle +'></div><div'+ loaderBgClass2 + loaderBgStyle +'></div></div>';
        } else if( loaderStyle == '13' ) {
            loaderStyleHtml = '<div class="css3-spinner-ball-pulse-sync"><div'+ loaderBgClass2 + loaderBgStyle +'></div><div'+ loaderBgClass2 + loaderBgStyle +'></div><div'+ loaderBgClass2 + loaderBgStyle +'></div></div>';
        } else if( loaderStyle == '14' ) {
            loaderStyleHtml = '<div class="css3-spinner-scale-ripple"><div'+ loaderBorderClass2 + loaderBorderStyle +'></div><div'+ loaderBorderClass2 + loaderBorderStyle +'></div><div'+ loaderBorderClass2 + loaderBorderStyle +'></div></div>';
        }

        $wrapper.animsition({
            inClass : animationIn,
            outClass : animationOut,
            inDuration : Number(durationIn),
            outDuration : Number(durationOut),
            linkElement : '#primary-menu ul li a:not([target="_blank"]):not([href*=#]):not([data-lightbox])',
            loading : true,
            loadingParentElement : 'body',
            loadingClass : 'css3-spinner',
            loadingHtml : loaderStyleHtml,
            unSupportCss : [
                'animation-duration',
                '-webkit-animation-duration',
                '-o-animation-duration'
            ],
            overlay : false,
            overlayClass : 'animsition-overlay-slide',
            overlayParentElement : 'body',
            timeOut: loaderTimeOut
        });



});

